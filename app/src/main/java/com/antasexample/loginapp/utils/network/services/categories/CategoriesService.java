package com.antasexample.loginapp.utils.network.services.categories;

import com.antasexample.loginapp.utils.network.entities.Category;

import retrofit2.Call;
import retrofit2.http.GET;

public interface CategoriesService {
    @GET("categories.php")
    Call<Category> getCategories();
}
