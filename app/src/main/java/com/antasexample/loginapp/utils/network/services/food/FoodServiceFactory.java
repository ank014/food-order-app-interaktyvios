package com.antasexample.loginapp.utils.network.services.food;

import com.antasexample.loginapp.utils.network.services.categories.CategoriesService;

/**
 * Created by Edvinas on 10/12/2017.
 */

public interface FoodServiceFactory {
    FoodService createService();
}
