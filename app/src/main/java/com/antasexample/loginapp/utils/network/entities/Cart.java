package com.antasexample.loginapp.utils.network.entities;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Edvinas on 10/12/2017.
 */

public class Cart {
    @SerializedName("result")
    @Expose
    private List<Cart.Result> result = new ArrayList<>();

    public List<Cart.Result> getResult() {
        return result;
    }

    public class Result {
        @SerializedName("id")
        @Expose
        private String id;
        private String category;
        private String name;
        private String image;
        private String price;
        @SerializedName("is_available")
        @Expose
        private String isAvailable;
        private boolean success;

        public String getId() {
            return id;
        }

        public String getCategory() {
            return category;
        }

        public String getName() {
            return name;
        }

        public String getImage() {
            return image;
        }

        public String getPrice() {
            return price;
        }

        public String getIsAvailable() {
            return isAvailable;
        }

        public boolean isSuccess() {
            return success;
        }
    }
}
