package com.antasexample.loginapp.utils.network.services.order;

/**
 * Created by Edvinas on 10/12/2017.
 */

public interface OrderStatusServiceFactory {
    OrderStatusService createService();
}
