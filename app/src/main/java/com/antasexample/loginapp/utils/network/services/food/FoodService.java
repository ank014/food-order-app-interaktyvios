package com.antasexample.loginapp.utils.network.services.food;

import com.antasexample.loginapp.utils.network.entities.Food;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by Edvinas on 10/12/2017.
 */

public interface FoodService {
    @GET("food.php")
    Call<Food> getFood();
}
