package com.antasexample.loginapp.utils.network;

import android.content.Context;

import com.antasexample.loginapp.utils.BaseApplication;

import retrofit2.Retrofit;

public class RetrofitProvider {
    public Retrofit getRetrofit(Context context) {
        return ((BaseApplication) context.getApplicationContext()).getRetrofit();
    }
}
