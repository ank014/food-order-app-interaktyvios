package com.antasexample.loginapp.utils.hash;

import android.util.Base64;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Random;

/**
 * Created by antas on 2016-11-29.
 */

public class PasswordHash {

    private static final Random RANDOM = new SecureRandom();

    /**
     *
     * @String pass Password to be hashed.
     * @return String
     * @throws Exception
     */
    public static String doTigerHashCheck(String pass) throws Exception
    {
        byte[] bytesOfMessage = pass.getBytes("UTF-8");

        MessageDigest md = MessageDigest.getInstance("MD5");
         byte[] theDigest = md.digest(bytesOfMessage);
        String hashPassword = Base64.encodeToString(theDigest, 1);

        return  hashPassword;

    }

    private static byte[] getNextSalt()
    {
        byte[] salt = new byte[16];
        RANDOM.nextBytes(salt);
        return salt;
    }

    /**
     * Returns password, hashed by SHA-512 and salted
     *
     * TODO: check if salt really works every time. Doesn't work. Fix it or don't use it
     *
     * @String passwordToHash Password to be hashed
     * @return String
     */
    public static String get_SHA_512_SecurePassword(String passwordToHash){
        String generatedPassword = null;
        try {
            MessageDigest md = MessageDigest.getInstance("SHA-512");
            md.update(getNextSalt());
            byte[] bytes = md.digest(passwordToHash.getBytes("UTF-8"));
            StringBuilder sb = new StringBuilder();
            for (int i=0; i< bytes.length ;i++) {
                sb.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
            }
            generatedPassword = sb.toString();
        }
        catch (NoSuchAlgorithmException | UnsupportedEncodingException e){
            e.printStackTrace();
        }
        return generatedPassword;
    }

}
