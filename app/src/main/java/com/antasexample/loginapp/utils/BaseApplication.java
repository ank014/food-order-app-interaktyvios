package com.antasexample.loginapp.utils;

import android.app.Application;
import android.content.Context;

import com.antasexample.loginapp.utils.network.DefaultReftrofitBuilder;

import retrofit2.Retrofit;

public class BaseApplication extends Application {
    private Retrofit retrofit;

    public static Retrofit getRetrofit(Context context) {
        return ((BaseApplication) context.getApplicationContext()).retrofit;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        retrofit = new DefaultReftrofitBuilder().buildRetrofit();
    }

    public Retrofit getRetrofit() {
        return retrofit;
    }
}