package com.antasexample.loginapp.utils.network.entities;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class Category {
    @SerializedName("result")
    @Expose
    private List<Result> result = new ArrayList<>();

    public List<Result> getResult() {
        return result;
    }

    public class Result {
        private String id;
        @SerializedName("parent_id")
        @Expose
        private String parentId;
        private String name;
        private String description;

        public Result(String id, String parentId, String name, String description) {
            this.id = id;
            this.parentId = parentId;
            this.name = name;
            this.description = description;
        }

        public String getId() {
            return id;
        }

        public String getParentId() {
            return parentId;
        }

        public String getName() {
            return name;
        }

        public String getDescription() {
            return description;
        }
    }
}
