package com.antasexample.loginapp.utils.network.services.categories;

public interface CategoriesServiceFactory {
    CategoriesService createService();
}
