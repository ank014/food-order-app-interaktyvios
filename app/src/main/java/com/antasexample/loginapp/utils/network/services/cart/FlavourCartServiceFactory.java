package com.antasexample.loginapp.utils.network.services.cart;

import com.antasexample.loginapp.utils.network.services.categories.CategoriesService;

import retrofit2.Retrofit;

/**
 * Created by Edvinas on 10/12/2017.
 */

public class FlavourCartServiceFactory implements CartServiceFactory {
    private final Retrofit retrofit;

    public FlavourCartServiceFactory(Retrofit retrofit) {
        this.retrofit = retrofit;
    }

    @Override
    public CartService createService() {
        return retrofit.create(CartService.class);
    }
}
