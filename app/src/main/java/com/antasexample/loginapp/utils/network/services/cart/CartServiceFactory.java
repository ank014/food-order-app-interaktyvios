package com.antasexample.loginapp.utils.network.services.cart;

import com.antasexample.loginapp.utils.network.services.categories.CategoriesService;

/**
 * Created by Edvinas on 10/12/2017.
 */

public interface CartServiceFactory {
    CartService createService();
}
