package com.antasexample.loginapp.utils.network.services.cart;

import retrofit2.Retrofit;

/**
 * Created by Edvinas on 10/12/2017.
 */

public class FlavourAddToCartServiceFactory implements AddToCartServiceFactory{
    private final Retrofit retrofit;

    public FlavourAddToCartServiceFactory(Retrofit retrofit) {
        this.retrofit = retrofit;
    }

    @Override
    public AddToCartService createService() {
        return retrofit.create(AddToCartService.class);
    }
}
