package com.antasexample.loginapp.utils.network.services.cart;

import com.antasexample.loginapp.utils.network.entities.AddToCartResponse;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * Created by Edvinas on 10/12/2017.
 */

public interface AddToCartService {
    @FormUrlEncoded
    @POST("toCart.php")
    Call<AddToCartResponse> makeOrder(
            @Field("id") int id,
            @Field("cusID") int customerId,
            @Field("address") String address
    );
}
