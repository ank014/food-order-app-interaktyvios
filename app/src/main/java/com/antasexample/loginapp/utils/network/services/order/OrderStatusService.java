package com.antasexample.loginapp.utils.network.services.order;

import com.antasexample.loginapp.utils.network.entities.AddToCartResponse;
import com.antasexample.loginapp.utils.network.entities.OrderStatus;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * Created by Edvinas on 10/12/2017.
 */

public interface OrderStatusService {
    @FormUrlEncoded
    @POST("orderStatus.php")
    Call<OrderStatus> getUserCart(@Field("id") int customerId);

}
