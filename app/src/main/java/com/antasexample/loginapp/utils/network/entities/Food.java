package com.antasexample.loginapp.utils.network.entities;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Edvinas on 10/12/2017.
 */

public class Food {
    @SerializedName("result")
    @Expose
    private List<Result> result = null;

    public List<Result> getResult() {
        return result;
    }

    public class Result {
        private String id;
        private String category;
        private String name;
        private String description;
        private Object image;
        private String price;
        @SerializedName("is_available")
        @Expose
        private String isAvailable;

        public Result(String id, String category, String name, String description,
                      Object image, String price, String isAvailable) {
            this.id = id;
            this.category = category;
            this.name = name;
            this.description = description;
            this.image = image;
            this.price = price;
            this.isAvailable = isAvailable;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getCategory() {
            return category;
        }

        public String getName() {
            return name;
        }

        public String getDescription() {
            return description;
        }

        public Object getImage() {
            return image;
        }

        public String getPrice() {
            return price;
        }

        public String getIsAvailable() {
            return isAvailable;
        }
    }
}