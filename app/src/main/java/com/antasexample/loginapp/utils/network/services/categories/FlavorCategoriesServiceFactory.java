package com.antasexample.loginapp.utils.network.services.categories;

import retrofit2.Retrofit;

public class FlavorCategoriesServiceFactory implements CategoriesServiceFactory {
    private final Retrofit retrofit;

    public FlavorCategoriesServiceFactory(Retrofit retrofit) {
        this.retrofit = retrofit;
    }

    @Override
    public CategoriesService createService() {
        return retrofit.create(CategoriesService.class);
    }
}
