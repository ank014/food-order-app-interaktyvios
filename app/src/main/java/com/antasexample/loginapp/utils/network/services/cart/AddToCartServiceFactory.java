package com.antasexample.loginapp.utils.network.services.cart;

/**
 * Created by Edvinas on 10/12/2017.
 */

public interface AddToCartServiceFactory {
    AddToCartService createService();
}
