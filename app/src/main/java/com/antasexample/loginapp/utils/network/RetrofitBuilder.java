package com.antasexample.loginapp.utils.network;

import retrofit2.Retrofit;

public interface RetrofitBuilder {
    Retrofit buildRetrofit();
}