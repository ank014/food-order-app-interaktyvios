package com.antasexample.loginapp.utils.network.services.cart;

import com.antasexample.loginapp.utils.network.entities.Cart;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * Created by Edvinas on 10/12/2017.
 */

public interface CartService {
    @FormUrlEncoded
    @POST("cart.php")
    Call<Cart> getUserCart(@Field("id") int customerId);
}
