package com.antasexample.loginapp.utils.network.entities;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Edvinas on 10/12/2017.
 */

public class OrderStatus {
    @SerializedName("result")
    @Expose
    private List<OrderStatus.Result> result = new ArrayList<>();

    public List<OrderStatus.Result> getResult() {
        return result;
    }

    public class Result {
        @SerializedName("name")
        @Expose
        private String dishName;
        private int status;

        public String getDishName() {
            return dishName;
        }

        public int getStatus() {
            return status;
        }
    }
}
