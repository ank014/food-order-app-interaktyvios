package com.antasexample.loginapp.utils.network.services.order;

import retrofit2.Retrofit;

/**
 * Created by Edvinas on 10/12/2017.
 */

public class FlavourOrderStatusServiceFactory implements OrderStatusServiceFactory {
    private final Retrofit retrofit;

    public FlavourOrderStatusServiceFactory(Retrofit retrofit) {
        this.retrofit = retrofit;
    }

    @Override
    public OrderStatusService createService() {
        return retrofit.create(OrderStatusService.class);
    }
}
