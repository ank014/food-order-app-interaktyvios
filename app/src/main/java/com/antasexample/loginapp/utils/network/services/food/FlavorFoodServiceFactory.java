package com.antasexample.loginapp.utils.network.services.food;

import com.antasexample.loginapp.utils.network.services.categories.CategoriesService;

import retrofit2.Retrofit;

/**
 * Created by Edvinas on 10/12/2017.
 */

public class FlavorFoodServiceFactory implements FoodServiceFactory{
    private final Retrofit retrofit;

    public FlavorFoodServiceFactory(Retrofit retrofit) {
        this.retrofit = retrofit;
    }

    @Override
    public FoodService createService() {
        return retrofit.create(FoodService.class);
    }
}
