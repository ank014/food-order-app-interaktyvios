package com.antasexample.loginapp.register;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.toolbox.Volley;
import com.antasexample.loginapp.BaseActivity;
import com.antasexample.loginapp.R;
import com.antasexample.loginapp.login.LoginActivity;
import com.antasexample.loginapp.utils.hash.PasswordHash;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import butterknife.BindView;

public class RegisterActivity extends BaseActivity {
    @BindView(R.id.name_edit_text)
    EditText etName;
    @BindView(R.id.username_edit_text)
    EditText etUsername;
    @BindView(R.id.password_edit_text)
    EditText etPassword;
    @BindView(R.id.email_edit_text)
    EditText etEmail;
    @BindView(R.id.register_button)
    Button bRegister;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setUpRegisterOnClick();
    }

    @Override
    protected int contentViewLayoutRes() {
        return R.layout.activity_register;
    }

    private void setUpRegisterOnClick() {
        bRegister.setOnClickListener(view -> {
            final String name = etName.getText().toString();
            final String username = etUsername.getText().toString();
            final String password = etPassword.getText().toString();
            final String email = etEmail.getText().toString();
            Response.Listener<String> responseListener = getResponse();
            if (name.isEmpty())
                Toast.makeText(getApplicationContext(), "Neįvestas vardas", Toast.LENGTH_SHORT).show();
            if (username.isEmpty()) {
                Toast.makeText(getApplicationContext(), "Neįvestas vartotojo vardas", Toast.LENGTH_SHORT).show();
            }
            String hashedPassword = null;
            String pattern = "(?=.*[0-9])(?=.*[a-z])(?=\\S+$).{8,}";
            try {
                if (password.matches(pattern)) {
                    hashedPassword = PasswordHash.doTigerHashCheck(password);
                } else {
                    AlertDialog.Builder builder = new AlertDialog.Builder(this);
                    builder.setMessage("Blogai įvestas slaptažodis!\n" +
                            "Jis turi susidėti iš 8 simbolių, bent vieno skaičiaus ir be tarpų.")
                            .setNegativeButton("Bandyti iš naujo", null)
                            .create()
                            .show();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            if (!isEmailValid(email)) {
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setMessage("Blogai įvestas el. paštas !")
                        .setNegativeButton("Bandyti iš naujo", null)
                        .create()
                        .show();
                return;
            }
            RegisterRequest registerRequest = new RegisterRequest(
                    name,
                    username,
                    hashedPassword,
                    email,
                    responseListener
            );
            RequestQueue queue = Volley.newRequestQueue(this);
            queue.add(registerRequest);
        });
    }

    @NonNull
    private Response.Listener<String> getResponse() {
        return response -> {
            try {
                JSONObject jsonResponse = new JSONObject(response);
                boolean success = jsonResponse.getBoolean("success");
                if (success) {
                    startActivity(LoginActivity.createIntent(this));
                } else {
                    AlertDialog.Builder builder = new AlertDialog.Builder(this);
                    builder.setMessage("Registracija nepavyko")
                            .setNegativeButton("Bandyti iš naujo", null)
                            .create()
                            .show();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        };
    }

    public static boolean isEmailValid(final String mailAddress) {
        Pattern pattern;
        Matcher matcher;
        final String EMAIL_PATTERN =
                "^[_A-Za-z0-9-+]+(\\.[_A-Za-z0-9-]+)*@"
                        + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
        pattern = Pattern.compile(EMAIL_PATTERN);
        matcher = pattern.matcher(mailAddress);
        return matcher.matches();
    }
}
