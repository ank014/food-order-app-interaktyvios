package com.antasexample.loginapp;

import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import butterknife.ButterKnife;
import butterknife.Unbinder;

public abstract class BaseActivity extends AppCompatActivity {
    private Unbinder unbinder;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(contentViewLayoutRes());
        unbinder = ButterKnife.bind(this);
    }

    @LayoutRes
    protected abstract int contentViewLayoutRes();

    @Override
    protected void onDestroy() {
        unbinder.unbind();
        super.onDestroy();
    }
}