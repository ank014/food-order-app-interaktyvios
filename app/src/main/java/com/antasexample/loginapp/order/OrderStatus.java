package com.antasexample.loginapp.order;

/**
 * Created by Edvinas on 2017-01-17.
 */

public class OrderStatus {

    private String name,status;

    public OrderStatus(String name, String status) {
        this.setName(name);
        this.setStatus(status);
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    public String getStatus() {
        return status;
    }
    public void setStatus(String status) {
        this.status = status;
    }
}
