package com.antasexample.loginapp.order;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.widget.ListView;
import android.widget.Toast;

import com.antasexample.loginapp.BaseActivity;
import com.antasexample.loginapp.R;
import com.antasexample.loginapp.utils.network.RetrofitProvider;
import com.antasexample.loginapp.utils.network.entities.OrderStatus;
import com.antasexample.loginapp.utils.network.services.order.FlavourOrderStatusServiceFactory;
import com.antasexample.loginapp.utils.network.services.order.OrderStatusService;
import com.antasexample.loginapp.utils.network.services.order.OrderStatusServiceFactory;

import butterknife.BindView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

import static com.antasexample.loginapp.R.id.toolbar;

public class OrderStatActivity extends BaseActivity {
    @BindView(R.id.order_status_list_view)
    ListView listView;
    StatusAdapter statusAdapter;
    private OrderStatusService service;
    private String userId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setUpToolbar();
        initFields();
        getUserCart();
    }

    private void getUserCart() {
        service.getUserCart(Integer.parseInt(userId)).enqueue(new Callback<OrderStatus>() {
            @Override
            public void onResponse(Call<OrderStatus> call, Response<OrderStatus> response) {
                for (OrderStatus.Result result : response.body().getResult()) {
                    statusAdapter.add(result);
                }
            }

            @Override
            public void onFailure(Call<OrderStatus> call, Throwable t) {
                Toast.makeText(OrderStatActivity.this, "Įvyko klaida", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    protected int contentViewLayoutRes() {
        return R.layout.activity_order_stat;
    }

    private void initFields() {
        Retrofit retrofit = new RetrofitProvider().getRetrofit(this);
        OrderStatusServiceFactory orderStatusFactory =
                new FlavourOrderStatusServiceFactory(retrofit);
        service = orderStatusFactory.createService();
        SharedPreferences sharedPref = getApplication()
                .getSharedPreferences("userInfo", Context.MODE_PRIVATE);
        userId = sharedPref.getString("id", "");
        statusAdapter = new StatusAdapter(this, R.layout.row_status);
        listView.setAdapter(statusAdapter);
    }

    private void setUpToolbar() {
        Toolbar myToolbar = findViewById(toolbar);
        setSupportActionBar(myToolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setTitle("Užsakymo būsena");
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }
}