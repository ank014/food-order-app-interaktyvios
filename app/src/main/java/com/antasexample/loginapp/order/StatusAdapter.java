package com.antasexample.loginapp.order;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.antasexample.loginapp.R;
import com.antasexample.loginapp.utils.network.entities.OrderStatus;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Edvinas on 2017-01-17.
 */


public class StatusAdapter extends ArrayAdapter {
    List list = new ArrayList();

    public StatusAdapter(Context context, int resource) {
        super(context, resource);
    }

    public void add(OrderStatus.Result object) {
        super.add(object);
        list.add(object);
    }

    @Override
    public int getCount() {
        return list.size();
    }


    @Override
    public Object getItem(int position) {
        return list.get(position);
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row;
        row = convertView;
        CategoryHolder categoryHolder;
        if (row == null) {
            LayoutInflater layoutInflater = (LayoutInflater) this.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            row = layoutInflater.inflate(R.layout.row_status, parent, false);
            categoryHolder = new CategoryHolder();
            categoryHolder.statusDish = (TextView) row.findViewById(R.id.statusDish);
            categoryHolder.statusName = (TextView) row.findViewById(R.id.statusName);
            row.setTag(categoryHolder);

        } else {
            categoryHolder = (CategoryHolder) row.getTag();
        }

        OrderStatus.Result stat = (OrderStatus.Result) this.getItem(position);
        //categoryHolder.tx_id.setText(categories.getId());
        categoryHolder.statusDish.setText(stat.getDishName());
        String status;
        String geltona = "#fdd835";
        String zalia = "#4caf50";
        String raudona = "#ff3d00";
        switch (stat.getStatus()){
            case 1:
                status = "Nepatvirtinta";
                categoryHolder.statusName.setTextColor(Color.parseColor(geltona));
                break;
            case 2:
                status = "Patvirtinta";
                categoryHolder.statusName.setTextColor(Color.parseColor(zalia));
                break;

            case 3:
                status = "Atmesta";
                categoryHolder.statusName.setTextColor(Color.parseColor(raudona));
                break;
            default:
                status = "Nepatvirtinta";
                break;
        }
        categoryHolder.statusName.setText(status);
        //categoryHolder.tx_description.setText(categories.getDescription());

        return row;
    }

    static class CategoryHolder {
        TextView statusDish, statusName;

    }
}