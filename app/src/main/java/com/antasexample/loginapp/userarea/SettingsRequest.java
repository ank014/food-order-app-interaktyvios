package com.antasexample.loginapp.userarea;

import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Edvinas on 2017-01-16.
 */


public class SettingsRequest extends StringRequest {

    private static final String REGISTER_REQUEST_URL = "http://antsia.stud.if.ktu.lt/androidPHP/editProf.php";
    private Map<String, String> params;

    public SettingsRequest(String username, String name, String password, String id, String email, Response.Listener<String> listener){
        super(Method.POST, REGISTER_REQUEST_URL, listener, null);
        params = new HashMap<>();
        params.put("username", username);
        params.put("name", name);
        params.put("password", password);
        params.put("id", id);
        params.put("email", email + "");
    }

    @Override
    public Map<String, String> getParams() {
        return params;
    }
}
