package com.antasexample.loginapp.userarea;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.toolbox.Volley;
import com.antasexample.loginapp.BaseActivity;
import com.antasexample.loginapp.R;
import com.antasexample.loginapp.utils.hash.PasswordHash;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import butterknife.BindView;

import static com.antasexample.loginapp.R.id.toolbar;

public class SettingsActivity extends BaseActivity {
    private static final String EMAIL_PATTERN = "^[_A-Za-z0-9-+]+(\\.[_A-Za-z0-9-]+)*@"
            + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
    @BindView(R.id.username_edit_text)
    EditText etUsername;
    @BindView(R.id.email_edit_text)
    EditText etEmail;
    @BindView(R.id.name_edit_text)
    EditText etName;
    @BindView(R.id.etPass)
    EditText etPass;
    @BindView(R.id.etPassB)
    Button etpassB;

    public static boolean isEmailValid(final String mailAddress) {
        Pattern pattern;
        Matcher matcher;
        pattern = Pattern.compile(EMAIL_PATTERN);
        matcher = pattern.matcher(mailAddress);
        return matcher.matches();
    }

    @Override
    protected int contentViewLayoutRes() {
        return R.layout.activity_setttings;
    }

    public static Intent createIntent(Context context) {
        return new Intent(context, SettingsActivity.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setUpToolbar();
        SharedPreferences preferences = getSharedPreferences("userInfo", MODE_PRIVATE);
        String id = preferences.getString("id", "");
        onEtPassBClick(id);
    }

    private void setUpToolbar() {
        Toolbar myToolbar = findViewById(toolbar);
        setSupportActionBar(myToolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setTitle(R.string.main_drawer_label_settings);
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }

    private void onEtPassBClick(String id) {
        etpassB.setOnClickListener(view -> {
            String name1 = etName.getText().toString();
            String username1 = etUsername.getText().toString();
            String password = etPass.getText().toString();
            String email1 = etEmail.getText().toString();
            Response.Listener<String> responseListener = response -> {
                try {
                    JSONObject jsonResponse = new JSONObject(response);
                    boolean success = jsonResponse.getBoolean("success");
                    if (success) {
                        Toast.makeText(this, "Duomenys sėkmingai pakeisti", Toast.LENGTH_SHORT).show();
                        SharedPreferences sharedPref = getSharedPreferences("userInfo", Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = sharedPref.edit();
                        if (!username1.isEmpty())
                            editor.putString("username", username1);
                        if (!name1.isEmpty())
                            editor.putString("name", name1);
                        if (!email1.isEmpty())
                            editor.putString("email", email1);
                        editor.apply();

                    } else {
                        AlertDialog.Builder builder = new AlertDialog.Builder(this);
                        builder.setMessage("Duomenų keitimas nepavyko.")
                                .setNegativeButton("Bandyti iš naujo", null)
                                .create()
                                .show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            };
            String hashedPassword = "";
            String pattern = "(?=.*[0-9])(?=.*[a-z])(?=\\S+$).{8,}";
            if (!password.isEmpty()) {
                try {
                    if (password.matches(pattern)) {
                        hashedPassword = PasswordHash.doTigerHashCheck(password);
                    } else {
                        AlertDialog.Builder builder = new AlertDialog.Builder(this);
                        builder.setMessage("Blogai įvestas slaptažodis!\n" +
                                "Jis turi susidėti iš 8 simbolių, bent vieno skaičiaus ir be tarpų.")
                                .setNegativeButton("Bandyti iš naujo", null)
                                .create()
                                .show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            if (!email1.isEmpty()) {
                try {

                    if (!isEmailValid(email1)) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(this);
                        builder.setMessage("Blogai įvestas el. paštas !")
                                .setNegativeButton("Bandyti iš naujo", null)
                                .create()
                                .show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            SettingsRequest registerRequest = new SettingsRequest(
                    username1,
                    name1,
                    hashedPassword,
                    id,
                    email1,
                    responseListener
            );
            RequestQueue queue = Volley.newRequestQueue(SettingsActivity.this);
            queue.add(registerRequest);
        });
    }
}