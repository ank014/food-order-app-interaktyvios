package com.antasexample.loginapp.main;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.antasexample.loginapp.BaseActivity;
import com.antasexample.loginapp.R;
import com.antasexample.loginapp.food.FoodActivity;
import com.antasexample.loginapp.utils.network.RetrofitProvider;
import com.antasexample.loginapp.utils.network.entities.Category;
import com.antasexample.loginapp.utils.network.services.categories.CategoriesService;
import com.antasexample.loginapp.utils.network.services.categories.FlavorCategoriesServiceFactory;
import com.mikepenz.materialdrawer.Drawer;

import java.util.Locale;

import butterknife.BindView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class MainActivity extends BaseActivity {
    private Drawer drawer;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.welcome_message_text_view)
    TextView welcomeTextView;
    @BindView(R.id.categoriesListView)
    ListView categoriesListView;
    CategoryAdapter categoryAdapter;

    public static Intent createIntent(Context context) {
        return new Intent(context, MainActivity.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SharedPreferences preferences = getSharedPreferences("userInfo", Context.MODE_PRIVATE);
        String userName = preferences.getString("name", "");
        final String toolbarTitle = String.format(
                Locale.US,"%s %s%s", "Sveiki atvykę, ", userName, "!");
        setUpToolbar(toolbarTitle);
        setUpListViewAdapter();
        setUpDrawer();
        getCategories();
    }

    private void setUpToolbar(String toolbarTitle) {
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setTitle(toolbarTitle);
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

    }

    private void setUpDrawer() {
        MainDrawerBuilder builder = new MainDrawerBuilder();
        drawer = builder.build(this, toolbar);
    }

    @Override
    protected int contentViewLayoutRes() {
        return R.layout.activity_main;
    }

    private void setUpListViewAdapter() {
        categoryAdapter = new CategoryAdapter(this, R.layout.row_layout);
        categoriesListView.setAdapter(categoryAdapter);
    }

    private void getCategories() {
        Retrofit retrofit = new RetrofitProvider().getRetrofit(this);
        FlavorCategoriesServiceFactory factory = new FlavorCategoriesServiceFactory(retrofit);
        CategoriesService service = factory.createService();
        service.getCategories().enqueue(new Callback<Category>() {
            @Override
            public void onResponse(Call<Category> call, Response<Category> response) {
                for (Category.Result result : response.body().getResult()) {
                    categoryAdapter.add(result);
                    setListViewOnClickListener(response.body());
                }
            }

            @Override
            public void onFailure(Call<Category> call, Throwable t) {
                showErrorMessage();
            }
        });
    }

    private void setListViewOnClickListener(Category categories) {
        categoriesListView.setOnItemClickListener((arg0, view, position, id) -> {
            String categoryId = categories.getResult().get(((int) id)).getId();
            startFoodActivity(categoryId);
        });
    }

    private void startFoodActivity(String categoryName) {
        Intent intent = new Intent(this, FoodActivity.class);
        intent.putExtra("key.categoryName", categoryName);
        startActivity(intent);
    }

    private void showErrorMessage() {
        Toast.makeText(this, "Įvyko klaida", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen()) {
            drawer.closeDrawer();
        } else {
            super.onBackPressed();
        }
    }
}
