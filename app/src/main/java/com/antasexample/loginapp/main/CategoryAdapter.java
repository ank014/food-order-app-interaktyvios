package com.antasexample.loginapp.main;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.antasexample.loginapp.R;
import com.antasexample.loginapp.utils.network.entities.Category;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by antas on 2016-12-15.
 */

public class CategoryAdapter extends ArrayAdapter {
    private List<Category.Result> list = new ArrayList();

    public CategoryAdapter(Context context, int resource) {
        super(context, resource);
    }


    public void add(Category.Result category) {
        super.add(category);
        list.add(category);
    }

    @Override
    public int getCount() {
        return list.size();
    }


    @Override
    public Object getItem(int position) {
        return list.get(position);
    }


    @NonNull
    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {
        View row;
        row = convertView;
        CategoryHolder categoryHolder;
        if (row == null) {
            LayoutInflater layoutInflater = (LayoutInflater) this.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            row = layoutInflater.inflate(R.layout.row_layout, parent, false);
            categoryHolder = new CategoryHolder();
            categoryHolder.tx_name = row.findViewById(R.id.tx_name);
            row.setTag(categoryHolder);

        } else {
            categoryHolder = (CategoryHolder) row.getTag();
        }

        Category.Result category = (Category.Result) this.getItem(position);
        categoryHolder.tx_name.setText(category.getName());
        return row;
    }

    static class CategoryHolder {
        TextView tx_id, tx_name, tx_description;

    }
}
