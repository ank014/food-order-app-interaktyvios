package com.antasexample.loginapp.main;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.v7.widget.Toolbar;

import com.antasexample.loginapp.R;
import com.antasexample.loginapp.cart.CartActivity;
import com.antasexample.loginapp.login.LoginActivity;
import com.antasexample.loginapp.userarea.SettingsActivity;
import com.mikepenz.materialdrawer.AccountHeader;
import com.mikepenz.materialdrawer.AccountHeaderBuilder;
import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.DrawerBuilder;
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IDrawerItem;

public class MainDrawerBuilder {
    private static final int POSITION_MENU = 1;
    private static final int POSITION_ORDER = 2;
    private static final int POSITION_SETTINGS = 3;
    private static final int POSITION_LOGOUT = 4;
    private Drawer drawer;

    public Drawer build(Activity activity, Toolbar toolbar) {
        PrimaryDrawerItem menuItem = getPrimaryDrawerItem(
                R.string.main_drawer_label_menu,
                POSITION_MENU
        );
        PrimaryDrawerItem orderItem = getPrimaryDrawerItem(
                R.string.main_drawer_label_order,
                POSITION_ORDER
        );
        PrimaryDrawerItem settingsItem = getPrimaryDrawerItem(
                R.string.main_drawer_label_settings,
                POSITION_SETTINGS
        );
        PrimaryDrawerItem logoutItem = getPrimaryDrawerItem(
                R.string.main_drawer_label_logout,
                POSITION_LOGOUT
        );
        AccountHeader header = getAccountHeader(activity);
        drawer = new DrawerBuilder()
                .withActivity(activity)
                .withToolbar(toolbar)
                .withAccountHeader(header)
                .addDrawerItems(
                        menuItem,
                        orderItem,
                        settingsItem,
                        logoutItem
                )
                .withOnDrawerItemClickListener(getOnDrawerItemClickListener(activity))
                .withOnDrawerItemClickListener(getOnDrawerItemClickListener(activity))
                .withOnDrawerItemClickListener(getOnDrawerItemClickListener(activity))
                .withOnDrawerItemClickListener(getOnDrawerItemClickListener(activity))
                .build();
        return drawer;
    }

    @NonNull
    private Drawer.OnDrawerItemClickListener getOnDrawerItemClickListener(Activity activity) {
        return (view, position, drawerItem) -> {
            if (drawerItem != null) {
                setUpOnDrawerItemClicked(activity, drawerItem);
            }
            return false;
        };
    }

    private PrimaryDrawerItem getPrimaryDrawerItem(int drawerItemNameRes, int positionIdentifier) {
        return new PrimaryDrawerItem()
                .withIdentifier(positionIdentifier)
                .withName(drawerItemNameRes);
    }

    private AccountHeader getAccountHeader(Activity activity) {
        return new AccountHeaderBuilder()
                .withActivity(activity)
                .withHeaderBackground(R.drawable.material_drawer_badge)
                .build();
    }

    private void setUpOnDrawerItemClicked(Context context, IDrawerItem drawerItem) {
        if (drawerItem.getIdentifier() == POSITION_MENU) {
            drawer.closeDrawer();
        } else if (drawerItem.getIdentifier() == POSITION_ORDER) {
            context.startActivity(CartActivity.createIntent(context));
        } else if (drawerItem.getIdentifier() == POSITION_SETTINGS) {
            context.startActivity(SettingsActivity.createIntent(context));
        } else if (drawerItem.getIdentifier() == POSITION_LOGOUT) {
            logout(context);
        }
    }

    private void logout(Context context) {
        SharedPreferences cartPreferences =
                context.getSharedPreferences("cart", Context.MODE_PRIVATE);
        cartPreferences.edit().clear().apply();
        SharedPreferences settingsPreferences =
                context.getSharedPreferences("userInfo", Context.MODE_PRIVATE);
        settingsPreferences.edit().clear().apply();
        Intent intent = new Intent(context, LoginActivity.class)
                .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
    }

}
