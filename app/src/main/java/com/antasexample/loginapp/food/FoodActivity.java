package com.antasexample.loginapp.food;

import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.widget.ListView;
import android.widget.Toast;

import com.antasexample.loginapp.BaseActivity;
import com.antasexample.loginapp.R;
import com.antasexample.loginapp.utils.network.RetrofitProvider;
import com.antasexample.loginapp.utils.network.entities.Food;
import com.antasexample.loginapp.utils.network.services.food.FlavorFoodServiceFactory;
import com.antasexample.loginapp.utils.network.services.food.FoodService;
import com.antasexample.loginapp.utils.network.services.food.FoodServiceFactory;

import butterknife.BindView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

import static com.antasexample.loginapp.R.id.toolbar;

public class FoodActivity extends BaseActivity {
    @BindView(R.id.foodListView)
    ListView foodListView;
    String categoryName;
    FoodAdapter foodAdapter;

    @Override
    protected int contentViewLayoutRes() {
        return R.layout.display_listview_food;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setUpToolbar();
        foodAdapter = new FoodAdapter(this, R.layout.row_food);
        foodListView.setAdapter(foodAdapter);
        categoryName = getIntent().getExtras().getString("key.categoryName");
        getFood(this);
    }

    private void getFood(FoodActivity foodActivity) {
        Retrofit retrofit = new RetrofitProvider().getRetrofit(this);
        FoodServiceFactory factory = new FlavorFoodServiceFactory(retrofit);
        FoodService service = factory.createService();
        service.getFood().enqueue(new Callback<Food>() {
            @Override
            public void onResponse(Call<Food> call, Response<Food> response) {
                for (Food.Result result : response.body().getResult()) {
                    if (categoryName.equals(result.getCategory()))
                        foodAdapter.add(result);
                }
            }

            @Override
            public void onFailure(Call<Food> call, Throwable t) {
                Toast.makeText(foodActivity, "Įvyko klaida", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void setUpToolbar() {
        Toolbar myToolbar = findViewById(toolbar);
        setSupportActionBar(myToolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setTitle(R.string.food_title_toolbar);
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }
}