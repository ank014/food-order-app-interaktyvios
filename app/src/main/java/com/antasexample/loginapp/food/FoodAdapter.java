package com.antasexample.loginapp.food;

import android.content.Context;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.antasexample.loginapp.R;
import com.antasexample.loginapp.utils.network.entities.Food;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by antas on 2016-12-15.
 */

public class FoodAdapter extends ArrayAdapter {
    List<Food> foodList = new ArrayList<Food>();
    List list = new ArrayList();
    private static final String cart = "cart";
    private static final String FOOD_TAG = "FOOD_TAG";

    public FoodAdapter(Context context, int resource) {
        super(context, resource);
    }

    public void add(Food.Result object) {
        super.add(object);
        list.add(object);
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View row;
        row = convertView;
        FoodHolder foodHolder;

        if (row == null) {
            LayoutInflater layoutInflater = (LayoutInflater) this.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            row = layoutInflater.inflate(R.layout.row_food, parent, false);
            foodHolder = new FoodHolder();
            foodHolder.tx_name = row.findViewById(R.id.tx_name);
            foodHolder.tx_price = row.findViewById(R.id.tx_price);
            foodHolder.tx_button = row.findViewById(R.id.cartB);
            final Food.Result food = (Food.Result) this.getItem(position);
            foodHolder.tx_button.setOnClickListener(v -> {
                addInJSONArray(food);
            });

            row.setTag(foodHolder);
        } else {
            foodHolder = (FoodHolder) row.getTag();
        }
        Food.Result food = (Food.Result) this.getItem(position);
        foodHolder.tx_name.setText(food.getName());
        foodHolder.tx_price.setText(food.getPrice());
        return row;
    }

    static class FoodHolder {
        TextView tx_id, tx_category, tx_name, tx_price;
        Button tx_button;
    }

    private void addInJSONArray(Food.Result productToAdd) {



        Gson gson = new Gson();
        SharedPreferences sharedPref = getContext().getSharedPreferences(cart, Context.MODE_PRIVATE);

        String jsonSaved = sharedPref.getString(FOOD_TAG, "");
        String jsonNewproductToAdd = gson.toJson(productToAdd);

        JSONArray jsonArrayProduct = new JSONArray();

        try {
            if (jsonSaved.length() != 0) {
                jsonArrayProduct = new JSONArray(jsonSaved);
            }
            jsonArrayProduct.put(new JSONObject(jsonNewproductToAdd));
            Toast.makeText(getContext(),
                    productToAdd.getName() + " pridėta į krepšelį", Toast.LENGTH_SHORT).show();
        } catch (JSONException e) {
            e.printStackTrace();
            Toast.makeText(getContext(), "Įvyko klaida", Toast.LENGTH_SHORT).show();
        }

        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(FOOD_TAG, jsonArrayProduct.toString());
        editor.apply();
    }
}
