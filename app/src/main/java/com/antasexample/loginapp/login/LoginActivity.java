package com.antasexample.loginapp.login;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.toolbox.Volley;
import com.antasexample.loginapp.BaseActivity;
import com.antasexample.loginapp.R;
import com.antasexample.loginapp.main.MainActivity;
import com.antasexample.loginapp.register.RegisterActivity;
import com.antasexample.loginapp.utils.hash.PasswordHash;

import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.OnClick;

import static android.support.v7.app.AlertDialog.Builder;


public class LoginActivity extends BaseActivity {
    @BindView(R.id.username_edit_text)
    EditText usernameEditText;
    @BindView(R.id.password_edit_text)
    EditText passwordEditText;
    @BindView(R.id.login_button)
    Button loginButton;
    @BindView(R.id.register_text_view)
    TextView registerTextView;

    public static Intent createIntent(Context context) {
        return new Intent(context, LoginActivity.class);
    }

    @Override
    protected int contentViewLayoutRes() {
        return R.layout.activity_login;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SharedPreferences sharedPref = getApplication().getSharedPreferences("userInfo", Context.MODE_PRIVATE);
        final String token = sharedPref.getString("token", "");
        if (!token.isEmpty()){
            startActivity(new Intent(this, MainActivity.class));
            finish();
        }
    }

    @OnClick(R.id.login_button)
    void onLoginClicked() {
        String username = usernameEditText.getText().toString();
        String password = passwordEditText.getText().toString();

        Response.Listener<String> responseListener = response -> {
            try {
                JSONObject jsonResponse = new JSONObject(response);
                boolean success = jsonResponse.getBoolean("success");
                if (success) {
                    saveUserInformation(jsonResponse);
                    startMainActivity();
                } else {
                    showErrorDialog();
                }
            } catch (JSONException exception) {
                exception.printStackTrace();
            }
        };

        String hash = null;
        String pattern = "(?=.*[0-9])(?=.*[a-z])(?=\\S+$).{8,}";
        try {
            if (password.matches(pattern)) {
                hash = PasswordHash.doTigerHashCheck(password);
            } else if (password.isEmpty() || !password.matches(pattern)) {
                Builder builder = new Builder(
                        this,
                        R.style.Theme_AppCompat_DayNight_Dialog
                );
                showAlertDialog(builder);
            }
        } catch (Exception exception) {
            exception.printStackTrace();
        }
        LoginRequest loginRequest = new LoginRequest(username, hash, responseListener);
        RequestQueue queue = Volley.newRequestQueue(this);
        queue.add(loginRequest);
    }

    private void showAlertDialog(Builder builder) {
        builder.setMessage("Prisijungimo klaida!\n" +
                "Blogai įvestas arba neįvestas slaptažodis")
                .setNegativeButton("Bandyti dar kartą", null)
                .create()
                .show();
    }

    private void startMainActivity() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        finish();
    }

    private void showErrorDialog() {
        Builder builder = new Builder(this, R.style.Theme_AppCompat_DayNight_Dialog);
        builder.setMessage("Blogi prisijungimo duomenys")
                .setNegativeButton("Bandyti dar kartą", null)
                .create()
                .show();
    }

    private void saveUserInformation(JSONObject jsonResponse) throws JSONException {
        SharedPreferences sharedPref = getSharedPreferences("userInfo", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString("id", jsonResponse.getString("id"));
        editor.putString("username", jsonResponse.getString("username"));
        editor.putString("name", jsonResponse.getString("name"));
        editor.putString("email", jsonResponse.getString("email"));
        editor.putString("type", jsonResponse.getString("type"));
        editor.putString("password", jsonResponse.getString("password"));
        editor.putString("token", "leToken");
        editor.apply();
    }

    @OnClick(R.id.register_text_view)
    void onRegisterClicked() {
        Intent registerIntent = new Intent(this, RegisterActivity.class);
        startActivity(registerIntent);
    }
}