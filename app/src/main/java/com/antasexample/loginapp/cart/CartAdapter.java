package com.antasexample.loginapp.cart;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.antasexample.loginapp.R;
import com.antasexample.loginapp.utils.network.entities.Food;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by antas on 2016-12-15.
 */

public class CartAdapter extends ArrayAdapter {
    private static final String cart = "cart";
    private static final String FOOD_TAG = "FOOD_TAG";
    private List<Food.Result> foodList = new ArrayList<>();
    private List<Food.Result> list = new ArrayList<>();

    CartAdapter(Context context, int resource) {
        super(context, resource);
    }

    public void add(Food.Result object) {
        super.add(object);
        list.add(object);
    }
    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @NonNull
    @Override
    public View getView(final int position, View convertView, @NonNull ViewGroup parent) {
        View row;
        row = convertView;
        FoodHolder foodHolder = new FoodHolder();

        if(row == null){
            LayoutInflater layoutInflater = (LayoutInflater) this.getContext()
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            if (layoutInflater != null) {
                row = layoutInflater.inflate(R.layout.row_cart, parent, false);
                foodHolder = new FoodHolder();
                foodHolder.cartName = row.findViewById(R.id.cartName);
                foodHolder.cartPrice = row.findViewById(R.id.cartPrice);
                foodHolder.cartRemove = row.findViewById(R.id.cartRemove);
                final Food.Result food = (Food.Result) this.getItem(position);
                if (food != null){
                    foodHolder.cartRemove.setOnClickListener(v -> {
                        foodList = getDataFromSharedPreferences();
                        foodList.remove(position);
                        Toast.makeText(getContext(), food.getName(), Toast.LENGTH_SHORT).show();
                        SharedPreferences settingsC = getContext().getSharedPreferences("cart", Context.MODE_PRIVATE);
                        settingsC.edit().clear().apply();
                        if (foodList != null){
                            for (int i = 0; i < foodList.size(); i++)
                                addInJSONArray(foodList.get(i));
                        }
                        Intent intent = new Intent(getContext(), CartActivity.class);
                        getContext().startActivity(intent);
                    });
                }
                row.setTag(foodHolder);
            }
        }
        else {
            foodHolder = (FoodHolder) row.getTag();
        }
        Food.Result food = (Food.Result) this.getItem(position);
        if (food != null) {
            foodHolder.cartName.setText(food.getName());
            foodHolder.cartPrice.setText(food.getPrice());
        }
        return row;
    }
    static class FoodHolder{
        TextView cartName, cartPrice;
        Button cartRemove;
    }
    private List<Food.Result> getDataFromSharedPreferences(){
        Gson gson = new Gson();
        List<Food.Result> productFromShared;
        SharedPreferences sharedPref = getContext().getSharedPreferences(cart, Context.MODE_PRIVATE);
        String jsonPreferences = sharedPref.getString(FOOD_TAG, "");

        Type type = new TypeToken<List<Food.Result>>() {}.getType();
        productFromShared = gson.fromJson(jsonPreferences, type);

        return productFromShared;
    }
    private void addInJSONArray(Food.Result productToAdd){

        Gson gson = new Gson();
        SharedPreferences sharedPref = getContext().getSharedPreferences(cart, Context.MODE_PRIVATE);

        String jsonSaved = sharedPref.getString(FOOD_TAG, "");
        String jsonNewproductToAdd = gson.toJson(productToAdd);

        JSONArray jsonArrayProduct = new JSONArray();

        try {
            if(jsonSaved.length()!=0){
                jsonArrayProduct = new JSONArray(jsonSaved);
            }
            jsonArrayProduct.put(new JSONObject(jsonNewproductToAdd));
        } catch (JSONException e) {
            e.printStackTrace();
        }

//SAVE NEW ARRAY
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(FOOD_TAG, jsonArrayProduct.toString());
        editor.commit();
    }

}
