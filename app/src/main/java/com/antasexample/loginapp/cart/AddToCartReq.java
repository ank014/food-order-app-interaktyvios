package com.antasexample.loginapp.cart;

import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Edvinas on 2017-01-15.
 */

public class AddToCartReq extends StringRequest {
    private static final String REGISTER_REQUEST_URL = "http://antsia.stud.if.ktu.lt/androidPHP/toCart.php";
    private Map<String, String> params;

    public AddToCartReq(String id, String cusID, String address, Response.Listener<String> listener){
        super(Method.POST, REGISTER_REQUEST_URL, listener, null);
        params = new HashMap<>();
        params.put("id", id);
        params.put("cusID", cusID);
        params.put("address",address);
    }

    @Override
    public Map<String, String> getParams() {
        return params;
    }

}
