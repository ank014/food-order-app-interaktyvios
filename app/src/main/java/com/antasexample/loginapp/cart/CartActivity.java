package com.antasexample.loginapp.cart;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.antasexample.loginapp.BaseActivity;
import com.antasexample.loginapp.R;
import com.antasexample.loginapp.main.MainActivity;
import com.antasexample.loginapp.order.OrderStatActivity;
import com.antasexample.loginapp.utils.network.RetrofitProvider;
import com.antasexample.loginapp.utils.network.entities.AddToCartResponse;
import com.antasexample.loginapp.utils.network.entities.Cart;
import com.antasexample.loginapp.utils.network.entities.Food;
import com.antasexample.loginapp.utils.network.services.cart.AddToCartService;
import com.antasexample.loginapp.utils.network.services.cart.AddToCartServiceFactory;
import com.antasexample.loginapp.utils.network.services.cart.CartService;
import com.antasexample.loginapp.utils.network.services.cart.CartServiceFactory;
import com.antasexample.loginapp.utils.network.services.cart.FlavourAddToCartServiceFactory;
import com.antasexample.loginapp.utils.network.services.cart.FlavourCartServiceFactory;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

import static com.antasexample.loginapp.R.id.toolbar;

public class CartActivity extends BaseActivity {
    private static final String cart = "cart";
    private static final String FOOD_TAG = "FOOD_TAG";
    private AddToCartService addToCartService;
    double cartSum = 0;
    List<Food.Result> foodList = new ArrayList<>();
    @BindView(R.id.cart_address_edit_text)
    EditText addressEditText;
    @BindView(R.id.cart_list_view)
    ListView listView;
    @BindView(R.id.cart_sum_text_view)
    TextView sumTextView;
    CartAdapter CartAdapter;

    public static Intent createIntent(Context context) {
        return new Intent(context, CartActivity.class);
    }

    @Override
    protected int contentViewLayoutRes() {
        return R.layout.display_listview_cart;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setUpToolbar();
        initFields();
        populateListView();
        DecimalFormat df = new DecimalFormat("#.##");
        sumTextView.setText(df.format(cartSum));
    }

    private void populateListView() {
        int id;
        String category;
        String name;
        String description;
        String image;
        double price;
        int is_available;
        if (foodList != null) {
            for (int i = 0; i < foodList.size(); i++) {
                id = Integer.parseInt(foodList.get(i).getId());
                category = foodList.get(i).getCategory();
                name = foodList.get(i).getName();
                description = foodList.get(i).getDescription();
                image = ((String) foodList.get(i).getImage());
                price = Double.parseDouble(foodList.get(i).getPrice());
                is_available = Integer.parseInt(foodList.get(i).getIsAvailable());
                cartSum += price;
                Food.Result food = new Food().new Result(String.valueOf(id), category, name,
                        description, image, String.valueOf(price), String.valueOf(is_available));
                CartAdapter.add(food);
            }
        } else {
            Toast.makeText(getApplicationContext(), "Krepšelis tuščias", Toast.LENGTH_SHORT)
                    .show();
        }
    }

    @OnClick(R.id.cart_back_button)
    void onBackClick() {
        startActivity(new Intent(this, MainActivity.class));
    }

    @OnClick(R.id.cart_order_status_button)
    void onOrderStatusClick() {
        startActivity(new Intent(this, OrderStatActivity.class));
    }

    @OnClick(R.id.cart_order_button)
    public void onOrderClick() {
        final String address = addressEditText.getText().toString();
        SharedPreferences sharedPref = getApplication().getSharedPreferences("userInfo", Context.MODE_PRIVATE);
        final String userId = sharedPref.getString("id", "");
        if (address.isEmpty()) {
            Toast.makeText(CartActivity.this, "Pirma įveskite adresą", Toast.LENGTH_SHORT).show();
            return;
        }
        foodList = getDataFromSharedPreferences();
        if (foodList != null) {
            for (int i = 0; i < foodList.size(); i++) {
                callMakeOrderRequest(address, userId, i);
            }
        } else
            Toast.makeText(CartActivity.this, "Krepšelis tuščias", Toast.LENGTH_SHORT).show();
    }

    private void callMakeOrderRequest(String address, String userId, int i) {
        addToCartService.makeOrder(
                Integer.parseInt(foodList.get(i).getId()),
                Integer.parseInt(userId),
                address
        ).enqueue(new Callback<AddToCartResponse>() {
            @Override
            public void onResponse(
                    Call<AddToCartResponse> call,
                    Response<AddToCartResponse> response
            ) {
                if (response.body().isSuccess()) {
                    Toast.makeText(getApplication(), "Užsakymas atliktas", Toast.LENGTH_SHORT).show();
                } else {
                    showMakeOrderAlertDialog();
                }

            }

            @Override
            public void onFailure(Call<AddToCartResponse> call, Throwable t) {
                Toast.makeText(CartActivity.this, "Įvyko klaida", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void showMakeOrderAlertDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(CartActivity.this);
        builder.setMessage("Užsakymas nepavyko, bandykite dar kartą")
                .setNegativeButton("Grįžti", null)
                .create()
                .show();
    }

    private void initFields() {
        CartAdapter = new CartAdapter(this, R.layout.row_food);
        listView.setAdapter(CartAdapter);
        foodList = getDataFromSharedPreferences();
        Retrofit retrofit = new RetrofitProvider().getRetrofit(this);
        AddToCartServiceFactory factory = new FlavourAddToCartServiceFactory(retrofit);
        addToCartService = factory.createService();
    }

    private List<Food.Result> getDataFromSharedPreferences() {
        Gson gson = new Gson();
        List<Food.Result> productFromShared;
        SharedPreferences sharedPref = getApplicationContext().getSharedPreferences(cart, Context.MODE_PRIVATE);
        String jsonPreferences = sharedPref.getString(FOOD_TAG, "");

        Type type = new TypeToken<List<Food.Result>>() {
        }.getType();
        productFromShared = gson.fromJson(jsonPreferences, type);

        return productFromShared;
    }
    private void setUpToolbar() {
        Toolbar myToolbar = findViewById(toolbar);
        setSupportActionBar(myToolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setTitle(R.string.main_drawer_label_order);
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }
}