package com.antasexample.loginapp.cart;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Edvinas on 2017-01-16.
 */

public class CartRequest extends StringRequest {

    private static final String LOGIN_REQUEST_URL = "http://antsia.stud.if.ktu.lt/androidPHP/cart.php";
    private Map<String, String> params;

    public CartRequest(String id, Response.Listener<String> listener){
        super(Request.Method.POST, LOGIN_REQUEST_URL, listener, null);
        params = new HashMap<>();
        params.put("id", id);
    }
    @Override
    public Map<String, String> getParams() {
        return params;
    }
}
