<?php

include_once("connections.php");
mysqli_set_charset($con, "utf8");

$statement = "SELECT * FROM dish";

$query = mysqli_query($con, $statement);
$response = array();

while ($row = mysqli_fetch_array($query)) {
	$response[] = array(
		"id" => $row['id'],
		"category" => $row['category'],
		"name" => $row['name'],
		"description" => $row['description'],
		"image" => $row['image'],
		"price" => $row['price'],
		"is_available" => $row['is_available']
	);
}

echo json_encode(array("result" => $response), JSON_UNESCAPED_UNICODE);
mysqli_close($con);
?>