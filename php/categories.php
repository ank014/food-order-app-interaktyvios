<?php

include_once("connections.php");	
mysqli_set_charset($con, "utf8");

$statement = "SELECT * FROM category";

$query = mysqli_query($con, $statement);
$response = array();

while ($row = mysqli_fetch_array($query)) {
	$response[] = array(
		"id" => $row['id'],
		"parent_id" => $row['parent_id'],
		"name" => $row['name'],
		"description" => $row['description']
	);
}

echo json_encode(array("result" => $response), JSON_UNESCAPED_UNICODE);
mysqli_close($con);