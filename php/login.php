<?php

    //Login for with hashed passwords
    
	include_once("connections.php");	
    $username = $_POST["username"];
    $password = $_POST["password"];

    $statement = mysqli_prepare($con, "SELECT * FROM user WHERE username = ?");
    mysqli_stmt_bind_param($statement, "s", $username);
    mysqli_stmt_execute($statement);
    mysqli_stmt_store_result($statement);
    mysqli_stmt_bind_result($statement, $colUserID, $colUsername, $colName, $colPassword, $colEmail, $colKey, $colType);
    
    $response = array();
    $response["success"] = false;  
    
    while(mysqli_stmt_fetch($statement)){
        if ($password == $colPassword) {
            $response["success"] = true; 
            $response["password"] = $colPassword; 
            $response["username"] = $colUsername;
            $response["id"] = $colUserID;
            $response["name"] = $colName;
            $response["type"] = $colType;
            $response["email"] = $colEmail;
        }
    }
    echo json_encode($response);
    mysqli_close($con);
?>