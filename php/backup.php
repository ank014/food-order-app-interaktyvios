package com.antasexample.loginapp;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
/**
 * Created by antas on 2016-12-15.
 */

public class FoodAdapter extends ArrayAdapter {

    List list = new ArrayList();
    public FoodAdapter(Context context, int resource) {
        super(context, resource);
    }

    public void add(Food object) {
        super.add(object);
        list.add(object);
    }
    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View row;
        row = convertView;
        FoodHolder foodHolder;

        if(row == null){
            LayoutInflater layoutInflater = (LayoutInflater) this.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            row = layoutInflater.inflate(R.layout.row_food, parent, false);
            foodHolder = new FoodHolder();
            //foodHolder.tx_id = (TextView) row.findViewById(R.id.tx_id);
            //foodHolder.tx_category = (TextView) row.findViewById(R.id.tx_category);
            foodHolder.tx_name = (TextView) row.findViewById(R.id.tx_name);
            foodHolder.tx_price = (TextView) row.findViewById(R.id.tx_price);
            foodHolder.tx_button = (Button) row.findViewById(R.id.cartB);
            final Food food2 = (Food) this.getItem(position);
            foodHolder.tx_button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    final String id = Integer.toString(food2.getId()) ;
                    SharedPreferences sharedPref = getContext().getSharedPreferences("userInfo", Context.MODE_PRIVATE);
                    SharedPreferences sharedPref2 = getContext().getSharedPreferences("addr", Context.MODE_PRIVATE);
                    if (sharedPref2.getString("address","").isEmpty())
                        Toast.makeText(getContext(), "Neira�ytas adresas !", Toast.LENGTH_SHORT).show();
                    else {
                        final String cusID = sharedPref.getString("id", "");
                        Response.Listener<String> responseListener = new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                try {
                                    JSONObject jsonResponse = new JSONObject(response);
                                    boolean success = jsonResponse.getBoolean("success");
                                    if (success) {
                                        Toast.makeText(getContext(), "idejo id" + id, Toast.LENGTH_SHORT).show();
                                    } else {
                                        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                                        builder.setMessage("Nepavyko papildyti krep�el�, bandykite dar kart�")
                                                .setNegativeButton("Gr��ti", null)
                                                .create()
                                                .show();
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }

                        };

                    AddToCartReq addToCartReq = new AddToCartReq(id, cusID, sharedPref2.getString("address",""), responseListener);

                    RequestQueue queue = Volley.newRequestQueue(getContext());
                    queue.add(addToCartReq);
                    //Toast.makeText(getContext(), id, Toast.LENGTH_SHORT).show();
                    }
                }
            });

            row.setTag(foodHolder);
        }
        else {
            foodHolder = (FoodHolder) row.getTag();
        }
        Food food = (Food) this.getItem(position);
        //foodHolder.tx_id.setText(Integer.toString(food.getId()));
        //foodHolder.tx_category.setText(food.getCategory());
        foodHolder.tx_name.setText(food.getName());
        foodHolder.tx_price.setText(Double.toString(food.getPrice()));
        return row;
    }
    static class FoodHolder{
        TextView tx_id, tx_category, tx_name, tx_price;
        Button tx_button;
    }
}
